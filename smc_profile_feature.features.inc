<?php
/**
 * @file
 * smc_profile_feature.features.inc
 */

/**
 * Implements hook_default_profile2_type().
 */
function smc_profile_feature_default_profile2_type() {
  $items = array();
  $items['member'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "member",
    "label" : "Member profile",
    "weight" : "0",
    "data" : { "registration" : 0 },
    "rdf_mapping" : []
  }');
  return $items;
}
